using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;


public class P1RV : MonoBehaviour {
	public GameObject obstacle;
	public float scaling = 1;
	public float offsetX = 0;
	public float offsetY = 0;
	public float offsetZ = 0;
	public float rotationX = 0;
	public float rotationY = 0;
	public float rotationZ = 0;
	public Material material;

	// Use this for initialization
	void Start () {
		string[][] points = Load("plans.txt");
		Debug.Log ("Nbr of points :" + points.Length);
		for (int i = 0; i < points.Length; i++) {
			Debug.Log (points [i][0] + "," + points [i][1] + "," + points [i][2]);
		}
		Color[] colorsList = {Color.black, Color.blue, Color.cyan, Color.gray, Color.green, Color.magenta, Color.red, Color.white, Color.yellow};
		Color[] colors = new Color[4];
		for (int i = 0; i < points.Length; i += 4) {
			GameObject myObstacle = Instantiate (obstacle, this.transform.GetChild(0));
			//GameObject myObstacle = Instantiate (obstacle, GameObject.Find("[immerseumPlayArea](Clone)").transform);
			myObstacle.transform.gameObject.AddComponent<MeshFilter> ();
			myObstacle.transform.gameObject.AddComponent<MeshRenderer> ();
			myObstacle.transform.gameObject.GetComponent<MeshRenderer> ().material = material;
			Mesh mesh = new Mesh ();
			mesh.name = "Obstacle";
			mesh.vertices = new Vector3[] {
				new Vector3 (float.Parse(points [i] [0]) + offsetX, float.Parse(points [i] [1]) + offsetY, float.Parse(points [i] [2]) + offsetZ),
				new Vector3 (float.Parse(points [i + 1] [0]) + offsetX, float.Parse(points [i + 1] [1]) + offsetY, float.Parse(points [i + 1] [2]) + offsetZ),
				new Vector3 (float.Parse(points [i + 2] [0]) + offsetX, float.Parse(points [i + 2] [1]) + offsetY, float.Parse(points [i + 2] [2]) + offsetZ),
				new Vector3 (float.Parse(points [i + 3] [0]) + offsetX, float.Parse(points [i + 3] [1]) + offsetY, float.Parse(points [i + 3] [2]) + offsetZ)
			};
			for (int j = 0; j < 4; j++) {
				int ran = UnityEngine.Random.Range (0, 9);
				colors [j] = colorsList[ran];
				Debug.Log (colors [j]);
			}
			mesh.triangles = new int[] { 0, 2, 1, 0, 3, 2, 0, 1, 2, 0, 2, 3, 2, 1, 3, 2, 3, 1, 0, 1, 3, 0, 3, 1};
			//mesh.uv = new Vector2[] { new Vector2(0,0), new Vector2(1,0), new Vector2(0,1), new Vector2(1,1) };
			//mesh.RecalculateNormals ();
			mesh.colors = colors;
			myObstacle.transform.GetComponent<MeshFilter> ().mesh = mesh;
		}
		this.transform.GetChild (0).transform.eulerAngles = new Vector3 (rotationX, rotationY, rotationZ);
		this.transform.GetChild (0).transform.localScale = new Vector3 (scaling, scaling, scaling);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private string[][] Load(string fileName)
	{
		string[][] points = {};
		try
		{
			string text;
			StreamReader myReader = new StreamReader(fileName, System.Text.Encoding.Default);
			Debug.Log(fileName);
			using (myReader)
			{
				text = myReader.ReadLine();	
				if (text != null)
				{
					string[] array = text.Split(' ');
					points = new string[array.Length/3][];
					for(int i = 0; i < array.Length; i+=3) {
						points[i/3] = new string[3];
						points[i/3][0] = array[i];
						points[i/3][1] = array[i+1];
						points[i/3][2] = array[i+2];
					}
				}
			}
		}
		catch (Exception e)
		{
			Console.WriteLine("{0}\n", e.Message);
		}
		return points;
	}
}
